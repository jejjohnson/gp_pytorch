import torch
import numpy as np
from torch.autograd import Variable
from torch import nn
from gp_pytorch.kernels import rbf_kernel
from gp_pytorch.functions import log_determinent
from sklearn.datasets import make_regression

class GP(nn.Module):
    def __init__(self, verbose=0, jitter=1e-3, epochs=100):
        super(GP, self).__init__()

        # Initialize the Mean Function
        self.mean = Variable(torch.zeros(1).type(torch.FloatTensor), requires_grad=True)

        # Initialize the Noise Likelihood
        self.noise = Variable(torch.ones(1).type(torch.FloatTensor), requires_grad=True)

        # Initialize the Theta (Signal Variance and Length Scale)
        self.theta = Variable(torch.ones(2).type(torch.FloatTensor), requires_grad=True)

        # add kernel function
        self.K = rbf_kernel

        self.verbose = verbose
        self.jitter = jitter
        self.epochs = epochs

    def fit(self, X, y):

        self.X_train_ = X
        self.y_train_ = y

        print(self.log_likelihood(X, y).data)

        for i in range(self.epochs):

            loss = self.log_likelihood(X, y)
            loss.backward()
            print(loss.data)

            # Update the parameters
            self.theta.data -= (self.jitter * self.theta.grad.data)
            self.theta.data.zero_()

        pass

    def log_likelihood(self, X, y):

        N = y.size(0)

        y_diff = (y - self.mean.expand_as(y))

        K = self.K(X, X, self.theta) + \
            torch.exp(self.noise).expand(N, N) * Variable(torch.eye(N))

        # Solve the Equation
        A = -0.5 * log_determinent(K)
        B = -0.5 * y_diff.dot(torch.mv(K.inverse(), y_diff))
        C = -0.5 * N * np.log(2 * np.pi)



        return - (A + B + C)





def main():
    np.random.seed(1)

    N = 12
    X = torch.rand(N, 1)
    Y = (torch.sin(12 * X) + 0.6 * torch.cos(25 * X) + torch.randn(N, 1) * 0.1 + 3).squeeze(1)

    # Load Model
    gp_model = GP(epochs=10)

    # Fit the Model
    gp_model.fit(X, Y)


    pass

if __name__ == '__main__':
    main()

