import torch
import numpy as np
from torch.autograd import Variable
from torch import nn

from sklearn.datasets import make_regression

# RBF Kernel
def rbf_kernel(X, Y, theta, diag=False):

    # Extract Parameters
    signal_variance = theta[0].exp()
    length_scale = theta[1].exp()

    diff = (X / length_scale).unsqueeze(1) - (Y / length_scale).unsqueeze(0)

    return signal_variance * diff.pow_(2).sum(-1).mul(-0.5).exp_()

def ard_kernel_normal(X, Y, theta):

    signal_variance = np.exp(theta[0])
    length_scale = np.exp(theta[1])

    D =  np.expand_dims(X / length_scale, 1) - np.expand_dims(Y / length_scale, 0)

    return signal_variance * np.exp(-0.5 * np.sum(D ** 2, axis=2))

def test_rbf():

    np.random.seed(1)

    X, y = make_regression(n_samples=100, n_features=20, n_informative=10, random_state=123)

    X = ((X - X.mean(axis=0)) / X.std(axis=0))
    X = np.hstack([np.ones((X.shape[0], 1)), X])

    K1 = ard_kernel_normal(X, X, [0.0, 0.0])

    X = Variable(torch.FloatTensor(X))
    y = Variable(torch.FloatTensor(y))


    # Initialize Parameters
    theta = Variable(torch.zeros(2).type(torch.FloatTensor), requires_grad=True)

    K2 = rbf_kernel(X, X, theta)

    K2 = K2.data.numpy()

    np.testing.assert_array_almost_equal(K1, K2)

    pass

def main():

    test_rbf()
    pass


if __name__ == '__main__':
    main()