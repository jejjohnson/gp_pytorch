import torch
import numpy as np
from torch.autograd import Variable
from torch import nn


class Cholesky(torch.autograd.Function):

    def forward(ctx, a):
        l = torch.potrf(a, False)
        ctx.save_for_backward(l)
        return l

    def backward(ctx, grad_output):
        l, = ctx.saved_tensors
        # Gradient is l^{-H} @ ((l^{H} @ grad) * (tril(ones)-1/2*eye)) @ l^{-1}
        # TODO: ideally, this should use some form of solve triangular instead of inverse...
        linv =  l.inverse()

        inner = torch.tril(torch.mm(l.t() ,grad_output) ) *torch.tril(1.0 -l.new(l.size(1)).fill_(0.5).diag())
        s = torch.mm(linv.t(), torch.mm(inner, linv))

        # could re-symmetrise
        # s = (s+s.t())/2.0

        return s

def log_determinent(M):
    return 2. * Cholesky()(M).diag().log().sum()

def numpy2var(x):
    return torch.autograd.Variable(torch.FloatTensor(x))

def var2numpy(x):
    return x.data.numpy()